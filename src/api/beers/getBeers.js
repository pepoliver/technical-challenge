import { collection, query, getDocs, orderBy } from 'firebase/firestore';

export default async function getBeers(db) {
  const beersSnapshot = await getDocs(
    query(collection(db, 'beers'), orderBy('order'))
  );
  const beers = [];
  beersSnapshot.forEach((doc) => {
    beers.push({
      id: doc.id,
      ...doc.data(),
    });
  });
  return beers;
}
