import { collection, addDoc } from 'firebase/firestore';

export default async function postBeers(db, list) {
  return await addDoc(collection(db, 'userBeers'), {
    list,
    date: new Date(),
  });
}
