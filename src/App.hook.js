import { createContext, useEffect, useState } from 'react';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyAUv5SQ82R_od53cwZ7uL8SMWPSmPPfcyg',
  authDomain: 'code-challenge-daf5e.firebaseapp.com',
  projectId: 'code-challenge-daf5e',
  storageBucket: 'code-challenge-daf5e.appspot.com',
  messagingSenderId: '964948200169',
  appId: '1:964948200169:web:80fa771890a1071e257e78',
};

export const AppContext = createContext();

export default function useApp() {
  const [state, setState] = useState({});

  useEffect(() => {
    const app = initializeApp(firebaseConfig);
    const db = getFirestore();
    setState({ app, db });
  }, []);

  return { state };
}
