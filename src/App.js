import Swiper from './components/Swiper/Swiper';
import { StyledApp } from './App.styled';
import useApp, { AppContext } from './App.hook';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBeer } from '@fortawesome/free-solid-svg-icons';

function App() {
  const { state } = useApp();

  return (
    <AppContext.Provider value={{ state }}>
      <StyledApp>
        <header className="App-header">
          <FontAwesomeIcon icon={faBeer} className="App-logo" />
        </header>
        <main>
          <Swiper />
        </main>
      </StyledApp>
    </AppContext.Provider>
  );
}

export default App;
