import { useContext, useEffect, useState } from 'react';
import getBeers from '../../api/beers/getBeers';
import postBeers from '../../api/beers/postBeers';
import { AppContext } from '../../App.hook';

const yesDirection = 'right';

export default function useSwiper({ cardRefs }) {
  const appContext = useContext(AppContext);
  const [state, setState] = useState({
    beers: [],
    loading: true,
    selectedBeers: [],
    finished: false,
  });

  useEffect(() => {
    async function init() {
      const db = appContext.state.db;
      if (db) {
        const beers = await getBeers(db);
        const beer = beers.shift();
        updateState({ beers, loading: false, beer, finished: false });
      }
    }
    init();
  }, [appContext]);

  useEffect(() => {
    async function sendData() {
      const db = appContext.state.db;
      await postBeers(db, state.selectedBeers);
      updateState({ sending: false });
    }
    if (state.finished) {
      sendData();
    }
  }, [state.finished, appContext.state.db, state.selectedBeers]);

  const actions = {
    swiped: (dir) => {
      const newState = {};
      if (dir === yesDirection) {
        const beerName = state.beer.name;
        newState.selectedBeers = [...state.selectedBeers, beerName];
      }
      newState.beers = state.beers;
      newState.beer = newState.beers.shift();

      if (!newState.beer) {
        newState.finished = true;
        newState.sending = true;
      }
      updateState(newState);
    },
    swipe: (id, dir) => {
      cardRefs.current[id].swipe(dir);
    },
  };

  function updateState(newState) {
    setState((previousState) => ({ ...previousState, ...newState }));
  }

  return { state, actions };
}
