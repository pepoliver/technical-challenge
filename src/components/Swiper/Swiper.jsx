import { useRef } from 'react';
import TinderCard from 'react-tinder-card';
import { faThumbsDown, faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useSwiper from './Swiper.hook.js';
import { StyledSwiper } from './Swiper.styled.js';
import Spinner from '../shared/Spinner/Spinner.jsx';

export default function Swiper() {
  const cardRefs = useRef({});
  const { state, actions } = useSwiper({ cardRefs });
  const { loading, beer, finished, sending } = state;
  return (
    <StyledSwiper>
      {loading && (
        <div className="loading">
          <Spinner />
        </div>
      )}
      {beer && (
        <>
          <TinderCard
            onSwipe={actions.swiped}
            key={beer.id}
            className="swipe"
            ref={(ref) => (cardRefs.current[beer.id] = ref)}
          >
            <div
              className="image-container"
              style={{ backgroundImage: `url(${beer.uri})` }}
            >
              <div className="name">{beer.name}</div>
            </div>
          </TinderCard>
          <div className="actions">
            <div
              className="icon ko"
              onClick={() => actions.swipe(beer.id, 'left')}
            >
              <FontAwesomeIcon icon={faThumbsDown} />
            </div>
            <div
              className="icon ok"
              onClick={() => actions.swipe(beer.id, 'right')}
            >
              <FontAwesomeIcon icon={faThumbsUp} />
            </div>
          </div>
        </>
      )}
      {finished && (
        <div className="finished">
          {sending ? (
            <>
              <h2>Sending your data...</h2>
              <Spinner />
            </>
          ) : (
            <>
              <h2>You finished!</h2>
              <p>Thanks for your time</p>
              <button
                onClick={() => {
                  window.location.reload();
                }}
              >
                Reload
              </button>
            </>
          )}
        </div>
      )}
    </StyledSwiper>
  );
}
