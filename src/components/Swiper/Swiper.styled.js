import styled from 'styled-components';

export const StyledSwiper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;

  .loading {
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .swipe {
    display: flex;
    flex-direction: column;
    flex: 1;
    margin: 0px 40px;

    .image-container {
      flex: 1;
      background-position: center;
      background-size: cover;
      border-radius: 20px;
      display: flex;
      flex-direction: column;
      justify-content: flex-end;
      padding: 20px;

      .name {
        font-size: 28px;
        font-weight: bold;
        text-shadow: 2px 2px #000;
      }
    }
  }
  .actions {
    flex: 0.3;
    display: flex;
    justify-content: space-between;
    padding-top: 20px;
    margin: 0px 40px;

    .icon {
      border-radius: 50%;
      width: 50px;
      height: 50px;
      display: flex;
      justify-content: center;
      align-items: center;

      &.ok {
        background-color: greenyellow;
      }

      &.ko {
        background-color: red;
      }
    }
  }
  .finished {
    display: flex;
    flex-direction: column;
    align-items: center;

    button {
      border: none;
      padding: 10px 20px;
      font-size: 20px;
      background-color: white;
      color: #000;
    }
  }
`;
