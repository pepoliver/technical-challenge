import styled from 'styled-components';

export const StyledSpinner = styled.div`
  .app-spinner {
    animation: app-spinner-spin infinite 2s linear;
  }

  @keyframes app-spinner-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;
