import { StyledSpinner } from './Spinner.styled';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Spinner() {
  return (
    <StyledSpinner>
      <FontAwesomeIcon icon={faSpinner} size="2x" className="app-spinner" />
    </StyledSpinner>
  );
}
